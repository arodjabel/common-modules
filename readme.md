Example Usage

```
const commonUtils = require('common-utils');
```
```
const toAddress = ['science@bill.nye'];
const fromAddress = 'et@phone.home';
const subject = 'New Hypothesis';
const emailText = 'Hey Bill, How are you?;
const clientSecret = './path/to/secret/here.json';
```

```
commonUtils.sendEmail(clientSecret, toAddress, fromAddress, subject, emailText);
```

You will need a G-Mail API token

source: https://developers.google.com/gmail/api/quickstart/nodejs